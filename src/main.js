import { createApp } from 'vue'
import App from './App.vue'
import element from '@/plugins/element.js'
import router from './router'
import store from './store'
import '@/styles/index.scss'
import Icons from '@/icons'
import i18n from '@/i18n/index.js'
import filter from '@/filter/index.js'
import directives from '@/directives'

import './permission.js'

const app = createApp(App)
app.use(element)
app.use(Icons)
app.use(store)
app.use(router)
app.use(i18n)
app.use(filter)
app.use(directives)
app.mount('#app')
