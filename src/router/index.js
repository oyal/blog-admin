import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layout'
import UserManageRouter from './modules/UserManage'
import RoleListRouter from './modules/RoleList'
import PermissionListRouter from './modules/PermissionList'
import ArticleRouter from './modules/Article'
import ArticleCreaterRouter from './modules/ArticleCreate'
import store from '@/store'

export const privateRoutes = [
  UserManageRouter,
  RoleListRouter,
  PermissionListRouter,
  ArticleRouter,
  ArticleCreaterRouter
]

export const publicRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login')
  },
  {
    path: '/',
    redirect: '/profile',
    component: Layout,
    children: [
      {
        path: '/profile',
        name: 'profile',
        component: () => import('@/views/profile'),
        meta: {
          title: 'profile',
          icon: 'user'
        }
      },
      {
        path: '/401',
        name: '401',
        component: () => import('@/views/error-page/401.vue')
      },
      {
        path: '/404',
        name: '404',
        component: () => import('@/views/error-page/404.vue')
      }
    ]
  }
]

export function resetRouter () {
  if (
    store.getters.userInfo &&
    store.getters.userInfo.permissions &&
    store.getters.userInfo.permissions.menus
  ) {
    const menus = store.getters.userInfo.permissions.menus
    menus.forEach(menu => {
      router.removeRoute(menu)
    })
  }
}

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: publicRoutes
})

export default router
