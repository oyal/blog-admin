import { setItem, getItem } from '@/utils/storage.js'
// import { TAGS_VIEW } from '@/constant/index.js'

export default {
  namespaced: true,
  state: () => ({
    sidebarOpened: true,
    language: getItem('LANG') || 'zh',
    tagsViewList: getItem('tagsView') || []
  }),
  mutations: {
    triggerSidebarOpened (state) {
      state.sidebarOpened = !state.sidebarOpened
    },
    setLanguage (state, lang) {
      setItem('LANG', lang)
      state.language = lang
    },
    addTagsViewList (state, tag) {
      const isFind = state.tagsViewList.find(item => {
        return item.path === tag.path
      })
      if (!isFind) {
        state.tagsViewList.push(tag)
        setItem('tagsView', state.tagsViewList)
      }
    },
    changeTagsView (state, {
      index,
      tag
    }) {
      state.tagsViewList[index] = tag
      setItem('tagsView', state.tagsViewList)
    },
    /**
     * 删除 tag
     * @param {type: 'other'||'right'||'index', index: index} payload
     */
    removeTagsView (state, payload) {
      if (payload.type === 'index') {
        state.tagsViewList.splice(payload.index, 1)
      } else if (payload.type === 'other') {
        state.tagsViewList.splice(
          payload.index + 1,
          state.tagsViewList.length - payload.index + 1
        )
        state.tagsViewList.splice(0, payload.index)
      } else if (payload.type === 'right') {
        state.tagsViewList.splice(
          payload.index + 1,
          state.tagsViewList.length - payload.index + 1
        )
      }
      setItem('tagsView', state.tagsViewList)
    }
  }
}
