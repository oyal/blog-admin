import { setItem, getItem } from '@/utils/storage.js'
import variables from '@/styles/variables.scss'

export default {
  namespaced: true,
  state: () => ({
    mainColor: getItem('MAIN_COLOR') || '#409eff',
    variables
  }),
  mutations: {
    setMainColor (state, newColor) {
      state.mainColor = newColor
      state.variables.menuBg = newColor
      setItem('MAIN_COLOR', newColor)
    }
  }
}
