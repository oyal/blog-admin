import { login, getUserInfo } from '@/api/user.js'
import md5 from 'md5'
import { setItem, getItem, removeAllItem } from '@/utils/storage.js'
import { resetRouter } from '@/router/index.js'

export default {
  namespaced: true,
  state: () => ({
    token: getItem('token') || '',
    userInfo: {}
  }),
  mutations: {
    setToken (state, token) {
      state.token = token
      setItem('token', token)
    },
    setUserInfo (state, userInfo) {
      state.userInfo = userInfo
    }
  },
  actions: {
    login ({ commit }, userInfo) {
      const {
        username,
        password
      } = userInfo
      return new Promise((resolve, reject) => {
        login({
          username: username.trim(),
          password: md5(password)
        }).then(data => {
          commit('setToken', data.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    async getUserInfo ({ commit }) {
      const { userInfo } = await getUserInfo()
      commit('setUserInfo', userInfo)
      return userInfo
    },
    logout ({ commit }) {
      resetRouter()
      commit('setToken', '')
      commit('setUserInfo', {})
      removeAllItem()
    }
  }
}
