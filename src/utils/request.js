import axios from 'axios'
import { ElMessage } from 'element-plus'
import store from '@/store/index.js'
import router from '@/router/index.js'

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 5000
}, error => {
  return Promise.reject(error)
})

service.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  config.headers['Accept-Language'] = store.getters.language
  return config
})

service.interceptors.response.use(res => {
  const {
    code,
    data,
    message
  } = res.data
  if (code === 200) {
    return data
  } else if (code === 401) {
    ElMessage.error(message)
    store.dispatch('user/logout')
    router.push('/login')
    return Promise.reject(new Error(message))
  } else {
    ElMessage.error(message)
    return Promise.reject(new Error(message))
  }
}, error => {
  console.log(error.message)
  ElMessage.error(error.message)
  return Promise.reject(error)
})

export default service
