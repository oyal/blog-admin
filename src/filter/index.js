import formatDate from '@/utils/formatDate.js'

export default app => {
  app.config.globalProperties.$filters = {
    formatDate
  }
}
