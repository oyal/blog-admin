import request from '@/utils/request.js'

export const login = data => {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export const getUserInfo = () => {
  return request({
    url: '/user/profile'
  })
}

export const getFeature = () => {
  return request({
    url: '/user/feature'
  })
}

export const getChapter = () => {
  return request({
    url: '/user/chapter'
  })
}

export const getUserList = query => {
  return request({
    url: '/user/list',
    params: query
  })
}

export const importBatchUser = data => {
  return request({
    url: '/user/batchImport',
    method: 'post',
    data
  })
}

export const deleteUser = data => {
  return request({
    url: '/user/delete',
    method: 'post',
    data
  })
}

export const getUserDetail = query => {
  return request({
    url: '/user/detail',
    params: query
  })
}

export const updateUserRoles = data => {
  return request({
    url: '/user/role',
    method: 'post',
    data
  })
}
