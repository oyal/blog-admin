import request from '@/utils/request.js'

export const getRoleList = query => {
  return request({
    url: '/role/list',
    params: query
  })
}

export const getRolePermission = query => {
  return request({
    url: '/role/permission',
    params: query
  })
}

export const distributePermission = data => {
  return request({
    url: '/role/distribute-permission',
    method: 'post',
    data
  })
}
