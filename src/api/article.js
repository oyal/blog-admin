import request from '@/utils/request.js'

export const getArticleList = query => {
  return request({
    url: '/article/list',
    params: query
  })
}

export const deleteArticle = data => {
  return request({
    url: '/article/delete',
    method: 'post',
    data
  })
}

export const getArticleDetail = query => {
  return request({
    url: '/article/detail',
    params: query
  })
}

export const createArticle = data => {
  return request({
    url: '/article/create',
    method: 'post',
    data
  })
}

export const editArticle = data => {
  return request({
    url: '/article/edit',
    method: 'post',
    data
  })
}
