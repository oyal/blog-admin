import request from '@/utils/request.js'

export const getPermissionList = () => {
  return request({
    url: '/permission/list'
  })
}
