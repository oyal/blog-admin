import router from '@/router/index.js'
import store from '@/store/index.js'

const whiteList = ['/login']

router.beforeEach(async (to, from, next) => {
  if (store.getters.token) {
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      if (!store.getters.hasUserInfo) {
        const { permissions } = await store.dispatch('user/getUserInfo')
        // permission = permission.map(item => item.permissionMark)
        const filterRoutes = await store.dispatch('permission/filterRoutes', permissions.menus)
        filterRoutes.forEach(item => {
          router.addRoute(item)
        })

        return next(to.path)
      }
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`)
    }
  }
})

router.afterEach(() => {

})
